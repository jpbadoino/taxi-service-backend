<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $request->user()->authorizeRoles(['admin']);
        return view('home');
    }

    public function anyData(){
        $users = User::whereHas('roles', function($q){
            $q->whereIn('name', ['driver']);
        });
        return Datatables::of($users)
        ->addIndexColumn()
        ->addColumn('deleteAction', function ($user) {
                  return '<form method="post"  action="'.route('deleteDriver', $user->id).'" id="form-delete-distributions-'.$user->id.'">
                          <a href="javascript:void(0)" class="data-delete" 
                                          data-form="distributions-'.$user->id.'">Delete'.
                               csrf_field().'
                          </a>
                      </form>';

            })
        ->rawColumns(['deleteAction'])
        ->make(true);
    }

    public function getCreateDriver(Request $request){
        $request->user()->authorizeRoles(['admin']);
        return view('new_driver');
    }

    public function createDriver(Request $request) {
        $request->user()->authorizeRoles(['admin']);
        $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email',
                'password' => 'required|string|min:6'
            ]);
        $driver = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $driver->roles()->attach(1);
        return redirect()->route('home')->with('success','Driver added succefully!');

    }

    public function deleteDriver(Request $request) {
        dd($request);
    }
}
