<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

//https://stackoverflow.com/questions/41547856/laravel-inter-user-messaging-system

class Message extends Model
{
    protected $fillable = ['body', 'sender_id', 'receiver_id'];

    // A message belongs to a sender
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    // A message also belongs to a receiver
    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }
}
