<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_driver = Role::where('name', 'driver')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $role_user = Role::where('name', 'user')->first();

        $driver = new User();
        $driver->name = 'Test Driver';
        $driver->email = 'driver@myapp.com';
        $driver->password = bcrypt('password');
        $driver->save();
        $driver->roles()->attach($role_driver);

        $admin = new User();
        $admin->name = 'App Admin';
        $admin->email = 'admin@myapp.com';
        $admin->password = bcrypt('123456');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'App User';
        $user->email = 'user@myapp.com';
        $user->password = bcrypt('qwerty');
        $user->save();
        $user->roles()->attach($role_user);
    }
}
